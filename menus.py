'''
Created on 3 dic. 2019

@author: eduardo@eduardograna.com
'''
from maya import cmds, mel

# get the main window name and set it as parent for the menu we will create afterwards
main_window_name = mel.eval("$temp1=$gMainWindow")
cmds.setParent(main_window_name)

my_menu_name = 'ill_mega_menu'

# check if it exists already, if it does, delete it
if cmds.control(my_menu_name, exists=True):
    cmds.deleteUI(my_menu_name, menu=True)

# add the menu
created_menu = cmds.menu(my_menu_name, label='Que Geta!', tearOff=True)

# add item with string command to be evaluated
cmds.menuItem(label='Super Cube', c="cmds.polyCube()")

# add divider
cmds.menuItem(divider=True)

# add item with string command to be evaluated
cmds.menuItem(label="Super Ball", c="cmds.polySphere()")


# define a custom function. WARNING! maya adds an argument that you need to include in your function
def function(stuff_maya_adds):
    cmds.confirmDialog(message=':) maya adds>' + str(stuff_maya_adds))


# add item with function command
cmds.menuItem(label="Super Prompt", c=function)

# optional but usefull ------------------------------------
# add to menu set!
# if not added to a menu set, it will allwasy be there,
# but if you define menu sets, you can have a different set of tools (a different menu)
# in each menu set


# find menu set name by display name
animation_menu_set = mel.eval('findMenuSetFromLabel("Animation")')

# set menu set as current to edit it
cmds.menuSet(currentMenuSet=animation_menu_set)

# add the menu to the menu set so it shows on the naimtion set only
cmds.menuSet(addMenu=created_menu)

# check that now the menu only appears when the menu set is set to 'Animation'
# (you have to change the menu set to refresh)


# list the menus
anim_menus = cmds.menuSet(animation_menu_set, query=True, menuArray=True)

for m in anim_menus:
    print m

# the last menu is the one we added, lets remove it

# remove the one we added from the set
cmds.menuSet(removeMenu=created_menu)

# now it will show all the time regardless of waht menu set is selected
#(you have to change the menu set to refresh)

# delete the menu for good!
cmds.deleteUI(created_menu, menu=True)





