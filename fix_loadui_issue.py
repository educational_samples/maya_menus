'''
Created on 18 dic. 2019

@author: eduardo
'''
from maya import cmds

# load ui as usual
r = cmds.loadUI(f=r'D:\work\eclipse_workspace\maya_menus\main_window.ui')

# move the ui 100 pixels to the right and 100 pixels down
cmds.window(r, e=1, te=100, le=100)

# now show window and it should be away from the margin
cmds.showWindow(r)
