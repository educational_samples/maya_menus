'''
Created on 18 dic. 2019

To run this inside maya:

folder = r'D:\work\eclipse_workspace'

import sys
sys.path.append(folder)

from maya_menus import parent_to_maya_window
reload(parent_to_maya_window)

#dialog will work
parent_to_maya_window.DialogUI()

# main window too
parent_to_maya_window.MainWindowUI()

# widget will not parent correctly because it tries
# to fit inside the maya main window
parent_to_maya_window.WidgetUI()




@author: eduardo
'''
import os

from Qt import QtWidgets, QtGui, QtCore  # @UnresolvedImport
import loadUiType

# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'main_window.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainWindowUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self):

        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(MainWindowUI, self).__init__(parent=parent)
        self.setupUi(self)

        # this will make the ui to show
        self.show()


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'dialog.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class DialogUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self):

        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(DialogUI, self).__init__(parent=parent)
        self.setupUi(self)

        # this will make the ui to show
        self.show()


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'widget.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class WidgetUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self):

        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(WidgetUI, self).__init__(parent=parent)
        self.setupUi(self)

        # this will make the ui to show
        self.show()



def getMayaMainWindow():
    ''' Tries to get the maya main window
    Args:
        None
    Returns:
        QWidget or None: the maya main window or None if it could not be found
    '''

    try:
        # we need to wrap a pointer as a pyside widget so we need help!
        from shiboken2 import wrapInstance
        from maya import OpenMayaUI as omui

        # get opinter object
        ptr = omui.MQtUtil.mainWindow()

        # cast pointer to widget (may fail)
        main_window = wrapInstance(long(ptr), QtWidgets.QWidget)

    except Exception, e:  # @UnusedVariable
        main_window = None

    return main_window
